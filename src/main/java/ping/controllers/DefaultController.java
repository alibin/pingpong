package ping.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ping.domain.User;
import ping.services.UserService;
import ping.vo.CommandRequest;
import ping.vo.PingRequest;
import ping.vo.PingResponse;

/**
 * @author qlibin@gmail.com
 *         Created on 1/28/15.
 */
@RestController
public class DefaultController {

	@Autowired
	private UserService userService;

	@RequestMapping("/handler")
	public ResponseEntity command(@RequestBody CommandRequest commandRequest) {
		if (commandRequest instanceof PingRequest)
			return processor((PingRequest) commandRequest);
		return new ResponseEntity(HttpStatus.BAD_REQUEST);
	}

	public ResponseEntity<PingResponse> processor(PingRequest pingRequest) {
		String userId = pingRequest.getUserId();
		User user = userService.ping(userId);
		PingResponse response = PingResponse.fromUser(user);
		return new ResponseEntity<PingResponse>(response, HttpStatus.OK);
	}

/*
	private final ConcurrentHashMap<String, AtomicLong> userCounters = new ConcurrentHashMap<String, AtomicLong>();
	private final AtomicLong counter = new AtomicLong();

	private Lock lock = new ReentrantLock();

	private Long countFor(String userId) {
		lock.lock();
		try {
			AtomicLong counter = userCounters.get(userId);
			if (counter == null) {
				counter = new AtomicLong();
				userCounters.put(userId, counter);
			}
			return counter.incrementAndGet();
		} finally {
			lock.unlock();
		}
	}
*/

}
