package ping.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import ping.domain.User;

import java.util.List;

/**
 * @author qlibin@gmail.com
 *         Created on 1/30/15.
 */
@Repository
public interface UserRepository extends MongoRepository<User, String> {

	List<User> findAllByUserId(String userId);
	User findByUserId(String userId);

}
