package ping.domain;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author qlibin@gmail.com
 *         Created on 1/30/15.
 */
@Document(collection = "users")
public class User extends AbstractEntity {

	private String userId;

	private Long pingCounter;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getPingCounter() {
		return pingCounter;
	}

	public void setPingCounter(Long pingCounter) {
		this.pingCounter = pingCounter;
	}
}
