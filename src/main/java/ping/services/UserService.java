package ping.services;

import com.mongodb.WriteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import ping.domain.User;
import ping.repositories.UserRepository;

/**
 * @author qlibin@gmail.com
 *         Created on 1/30/15.
 */
@Service
public class UserService {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private UserRepository userRepository;

	public synchronized User ping(String userId) {

		User user = mongoTemplate.findAndModify(
				new Query(Criteria.where("userId").is(userId)),
				new Update().inc("pingCounter", 1),
				new FindAndModifyOptions().returnNew(true),
				User.class);
		if (user == null) {
			return newUser(userId);
		}
		return user;
	}

	public User newUser(String userId) {
		User user = new User();
		user.setUserId(userId);
		user.setPingCounter(1l);
		userRepository.save(user);
		return user;
	}

}
