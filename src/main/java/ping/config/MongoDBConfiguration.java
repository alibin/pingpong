package ping.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author qlibin@gmail.com
 *         Created on 1/30/15.
 */
@Configuration
@EnableMongoRepositories(
		basePackages = {"ping.repositories.*"},
		createIndexesForQueryMethods = true
)
@EnableMongoAuditing(
		setDates = true
)
public class MongoDBConfiguration {
}
