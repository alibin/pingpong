package ping.vo;

import ping.domain.User;

/**
 * @author qlibin@gmail.com
 *         Created on 1/28/15.
 */
public class PingResponse {

	private String userId;
	private Long pingCounter;

	public PingResponse(String userId, Long pingCounter) {
		this.userId = userId;
		this.pingCounter = pingCounter;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getPingCounter() {
		return pingCounter;
	}

	public void setPingCounter(Long pingCounter) {
		this.pingCounter = pingCounter;
	}

	public static PingResponse fromUser(User user) {
		return new PingResponse(user.getUserId(), user.getPingCounter());
	}
}
