package ping.vo;

/**
 * @author qlibin@gmail.com
 *         Created on 1/28/15.
 */
public class PingRequest extends CommandRequest {
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}
