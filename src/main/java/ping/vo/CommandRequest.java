package ping.vo;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.HashMap;
import java.util.List;

/**
 * @author qlibin@gmail.com
 *         Created on 1/28/15.
 */
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		include = JsonTypeInfo.As.PROPERTY,
		property = "type"
)
@JsonSubTypes({
		@JsonSubTypes.Type(value = PingRequest.class, name = "PING")
//		,
//		@JsonSubTypes.Type(value = FooRequest.class, name = "FOO")
})
public abstract class CommandRequest {
}
